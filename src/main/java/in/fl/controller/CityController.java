package in.fl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CityController {

	@RequestMapping(value ="/city")
	public String sayHello (Model model) {
		model.addAttribute("greeting", "Hello World");
		return "masters/city";
	}
	
}
