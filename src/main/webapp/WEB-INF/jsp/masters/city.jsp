<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file = "../include/style.jsp" %>
<%@ include file = "../include/js.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>City</title>
</head>
<body>

<div class="container">
 
  <table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>ID</th>
        <th>Cnno</th>
      </tr>
    </thead>
    <tbody id="t_body" >
     
    </tbody>
  </table>
</div>

<script>
loadDoc();
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
//      console.log(this.responseText);
	 var resJson = JSON.parse(this.responseText);
     var data = resJson.body.resBody;
	 var htmlData = "";
	 var j = 0;
	 for(var i=0;i<data.length;i++){
	 j = i+1;
	 htmlData = htmlData + "<tr>"
				+"<td>" + j + "</td>"
				+"<td>" + data[i].id + "</td>"
				+"<td>" + data[i].cnno + "</td>"
				+"</tr>";
	 }
// 	 console.log(htmlData);
	 document.getElementById("t_body").innerHTML = htmlData;
	}
  };
  xhttp.open("POST", "https://hi4z6v902e.execute-api.us-east-2.amazonaws.com/default/javaDemoFunction", true);
  xhttp.send();
}
</script>

</body>
</html>